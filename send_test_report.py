import click
import pandas as pd
import boto3
from jinja2 import Template
from elasticsearch import Elasticsearch
from lxml import html

html_template= """
<!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Simple Call To Action</title>
  </head>
  <body class style="background-color: #000000; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
   
          <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: auto; padding: 10px;">
            <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #eedcdc; border-radius: 3px; width: auto;">
              <tr style="background-color: white;" bgcolor="white">
                <td class="align-center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                  <img src="https://www.indiantelevision.com/sites/default/files/images/tv-images/2020/07/24/khatabook.jpg" height="100" alt="Khatabook" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%;">
                </td>
              </tr>
              <tr style="background-color: #dae5f4;" bgcolor="#dae5f4">
                <td class="align-center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 10px;" valign="top">
                    Please check <a href="https://monitoring.stage.khatabook.com/d/qFQOcXrMz/automation-report?orgId=1{{ service_query }}" style="color: #ec0867; text-decoration: underline;"> Grafana Dashboard </a> For More Details
                </td>
              </tr>
            </table>
          </div>
<div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: auto; padding: 10px;">

<!-- START CENTERED WHITE CONTAINER -->

<table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; background: #eedcdc; border-radius: 3px; width: auto;">

              <!-- START MAIN CONTENT AREA -->
   {{ report_table }}
</table>

            <!-- START FOOTER -->
            <div class="footer" style="clear: both; margin-top: 10px; text-align: center; width: 100%;">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; min-width: 100%; width: 100%;" width="100%">
                <tr style="background-color: white;" bgcolor="white">
                  <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; color: #9a9ea6; font-size: 12px; text-align: center;" valign="top" align="center">
                    <span style="font-size: 12px; text-align: center; color: black;">Please contact Automation team for any issue</span>
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->

          <!-- END CENTERED WHITE CONTAINER -->
          </div>
  </body>
</html>
"""


def color_rows_for_builds_status(html_text, build_status,tests_status):
  doc = html.fromstring(html_text)
  btags = doc.xpath('//tbody/tr')
  for b,status,tstatus in zip(btags,build_status,tests_status):
    if status == "FAILURE":
      b.attrib["style"]="background-color: lightcoral; color: whitesmoke"
    elif tstatus == True:
      b.attrib["style"]="background-color: pink; color: whitesmoke"
    else:
      b.attrib["style"]="background-color: lightgreen; color: whitesmoke"
  return html.tostring(doc, pretty_print=True, encoding="utf-8")
  
    
    

def fetch_client():
    es = Elasticsearch("https://reporting-elastic-search.stage.khatabook.com")
    return es

def getRecordsFromElasticSearch(services):
    es = fetch_client()
    res = es.search(index="*service", body={
      "query": {
        "match_all": {}
      },
      "size": 50,
      "sort": [
        {
          "@timestamp": {
            "order": "desc"
          }
        }
      ]
    })
    df=pd.json_normalize(res["hits"]["hits"]) 
    if services:
      df=df[df["_index"].isin(services)]
    df["_index"]=df["_index"].str.replace("-service","").str.upper()
    df['_source.@timestamp']=pd.to_datetime(df['_source.@timestamp'])
    df=df.loc[df.groupby("_index")['_source.@timestamp'].idxmax()]
    df['_source.@timestamp'] = df['_source.@timestamp'].dt.strftime('%d/%m/%Y')
    mapping={
        "_index":"Service Name",
        "_source.build_status": "Build Status",
        "_source.time":"Build Time(sec)",
        "_source.build_url":"Build URL",
        "_source.total":"Total Tests",
        "_source.passPercentage":"Passed(%)",
        "_source.failPercentage":"Failed(%)",
        "_source.sonarQubeResults.component.coverage":"Code Coverage(%)",
        "_source.sonarQubeResults.component.code_smells":"Code Smells",
        "_source.snyk.high":"Vulnerabilites High",
        "_source.snyk.medium":"Vulnerabilites Medium",
        "_source.snyk.low":"Vulnerabilites Low",
        "_source.@timestamp":"Run Date",        
    }
    df=df[mapping.keys()].rename(mapping,axis=1)
    df["Total Tests"]=df["Total Tests"].astype("Int64")
    df["Vulnerabilites High"]=df["Vulnerabilites High"].astype("Int64")
    df["Vulnerabilites Medium"]=df["Vulnerabilites Medium"].astype("Int64")
    df["Vulnerabilites Low"]=df["Vulnerabilites Low"].astype("Int64")
    df["Code Smells"]=df["Code Smells"].astype("Int64")
    df["Passed(%)"]=df["Passed(%)"].round(decimals=1)
    df["Failed(%)"]=df["Failed(%)"].round(decimals=1)
    df["Build Time(sec)"]=df["Build Time(sec)"].round(decimals=1)

    return (df.to_html(border=True, render_links=True, table_id="report-table", na_rep="NA",index=False),df["Build Status"].tolist(),(df["Failed(%)"] != 0).tolist())

def send_email(report_table, target_emails,services):
    ses_client = boto3.client("ses", region_name="ap-south-1")
    CHARSET = "UTF-8" 
    services=["All"] if not services else services 
    service_query="".join(["&var-Report={}".format(x) for x in services])
    email_body= Template(html_template).render(report_table=report_table, service_query=service_query)
    if target_emails:
      response = ses_client.send_email(
          Destination={
              "ToAddresses": target_emails,
          },
          Message={
          "Body": {
              "Html": {
                  "Charset": CHARSET,
                  "Data": email_body,
              }
          },
          "Subject": {
              "Charset": CHARSET,
              "Data": "Tests overview reports",
          },
          },
          Source="adminqa@khatabook.com",
      )
      print(response)
    return email_body

@click.command()
@click.option("--target", multiple=True)
@click.option("--service", multiple=True)
@click.option("--save-html-path", type=str)
def send_report(target, service, save_html_path):
    html_table_string,build_statuses,tests_status = getRecordsFromElasticSearch(services=service)
    email_body = send_email(html_table_string, target_emails=target,services=service)
    email_body = color_rows_for_builds_status(email_body,build_statuses,tests_status)
    # Save body as an HTML file
    if save_html_path:
        with open(save_html_path, "wb") as f:
            f.write(email_body)

if __name__ == '__main__':
    send_report()